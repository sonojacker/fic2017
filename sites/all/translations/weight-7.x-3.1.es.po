# Spanish translation of Weight (7.x-3.1)
# Copyright (c) 2016 by the Spanish translation team
#
msgid ""
msgstr ""
"Project-Id-Version: Weight (7.x-3.1)\n"
"POT-Creation-Date: 2016-06-22 22:17+0000\n"
"PO-Revision-Date: YYYY-mm-DD HH:MM+ZZZZ\n"
"Language-Team: Spanish\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"

msgid "Weight"
msgstr "Peso"
msgid "Default"
msgstr "Predeterminado"
msgid "Fields"
msgstr "Campos"
msgid "Range"
msgstr "Rango"
