<article class="<?php print $classes; ?> comment clearfix comment-row "<?php print $attributes; ?>>
    
    <div class="author-comment">

    <p class="submitted">
      <?php
        print t('Submitted by !username on !datetime.',
          array('!username' => $author, '!datetime' => $created));
      ?>
    </p>
    <span class="subject"><?php print $content["comment_body"]["#object"]->subject ?></span>
    </div>

    <div class="content comment-text"<?php print $content_attributes; ?>>
        <?php // We hide the comments and links now so that we can render them later.
        hide($content['links']);
        print render($content); ?>
        <?php if ($signature): ?>
        <div class="user-signature clearfix">
        <?php print $signature ?>
        </div>
        <?php endif; ?>

    </div>
</article>
